class Problema (val enunciat : String, val jocProbesPublic : Pair<String, String>, val jocProbesPrivat : Pair<String, String>, var resolt : Boolean = false, var intents : Int = 0){
    /**
     * Mostra l'enunciat i els jocs de proba
     * @param no té parametres d'entrada
     * @return no té retorn només fa un print per pantalla
     * @author Pol Martinez Lopez
     * @version 1.0
     */
    fun mostraEnunciat() {
        println(enunciat)
        println("INPUT: ${jocProbesPublic.first} - OUTPUT: ${jocProbesPublic.second}")
        println("INPUT: ${jocProbesPrivat.first}")
    }

    /**
     * Suma 1 al intents
     * @param no té parametres d'entrada
     * @return no té retorn
     * @author Pol Martinez Lopez
     * @version 1.0
     */
    fun actualitzaIntents() {
        intents++
    }

    /**
     * Canvia l'estat de la variable resolt
     * @param no té parametres d'entrada
     * @return no té retorn
     * @author Pol Martinez Lopez
     * @version 1.0
     */
    fun canviaResolt() {
        resolt = !resolt
    }
}
