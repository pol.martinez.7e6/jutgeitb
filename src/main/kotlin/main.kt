/**
 * Funcio que demana a l'usuari que introdueixi Si o No per si vol acceptar el problema
 * @param no té paramatres d'entrada
 * @return retorna la eleccio de l'usuario que es Un String que conté Si o No
 * @author Pol Martinez Lopez
 * @version 1.0
 */
fun acceptarProblema(): String {
    println("Vols intentar fer aquest problema? Si / No")
    var eleccion = readln()
    while (eleccion != "No" && eleccion != "Si") {

        println("Vols intentar fer aquest problema? Si / No")
        eleccion = readln()
    }

    return eleccion
}

/**
 * Funcio que pregunta a l'usuari si vol tornar a intentar el problemes que no ha resolt
 * @param No té parametres d'entrada
 * @return retorna un String amb la eleccio de l'usuari
 * @author Pol Martinez Lopez
 * @version 1.0
 */
fun tornarJugar(): String {
    var seguirJugant: String

    println("Quieres intentar adivinar los problemas que no has adivinado? Si / No")
    seguirJugant = readln()
    while (seguirJugant != "No" && seguirJugant != "Si") {

        println("Quieres intentar adivinar los problemas que no has adivinado? Si / No")
        seguirJugant = readln()
    }

    return seguirJugant
}

fun main() {
    //En aquesta llista afegeixo el problemes resolts per mes tard mostrar els intents utilitzats per fer el problema.
    val llistaAcertats = mutableListOf<Problema>()
    //En aquesta llista creo tots els problemes que més tard seran mostrats al usuari.
    val llistaProblemes = mutableListOf(
        Problema(
            "Fes un programa que mostri el resultat de la suma de dos nombres enters.",
            Pair("6 7", "13"),
            Pair("5 6","11")
        ),

        Problema(
            "Fes un programa que mostri el resultat de la potencia de dos numeros enters.",
            Pair("2 3", "8"),
            Pair("4 5", "1024")
        ),

        Problema(
            "Fes un programa que repeteixi una paraula les vegades indicades",
            Pair("hola 2", "holahola"),
            Pair("Pol 4", "PolPolPolPol")
        ),

        Problema(
            "Fes un programa que retorni true si resto entre el primer nombre entre el segon és 0.",
            Pair("4 2", "true"),
            Pair("5 3", "false")
        ),

        Problema(
            "Fes un prgrama que mostri el numero mes gran dels dos introduits.",
            Pair("4 3", "4"),
            Pair("7 -1", "7")
        )
    )

    //va a hacer el programa mientras seguirJugant sea "Si"
    do {

        //Iterador que passa per tots els problemes que hi ha a la llista problemes
        for (problem in llistaProblemes) {

            //Si el problema no esta resolt que faci el seguent
            if (!problem.resolt) {

                //Mostra l'enunciat de problema i pregunta a l'usuari si vol jugar.
                problem.mostraEnunciat()
                println()
                //Pide al usuario si quiere intentar adivinar el problema
                val eleccion = acceptarProblema()
                //Cuando sea si que haga lo siguiente
                if (eleccion == "Si") {

                    //Funcionamiento prgrama general
                    do {

                        //Va sumant intents i comprobant la resposta de l'usuari per veure si l'acerta o no.
                        println("Quina es la resposta a aquest problema?")
                        val resposta = readln()
                        problem.actualitzaIntents()
                        //Acierta el resultado del problema
                        if (resposta == problem.jocProbesPrivat.second) {

                            problem.resolt = !problem.resolt
                        }
                    } while (!problem.resolt)


                    //Imprime la respuesta del programa y los intentos utilizados para acertarlo
                    println("La resposta correcta és ${problem.jocProbesPrivat.second}, l'has acertat en ${problem.intents}")
                    println("------------------------")
                    llistaAcertats.add(problem)
                }
            }
        }
    } while (tornarJugar() == "Si" && llistaAcertats.size < 5)

    //Aquest for recorre una nova llista amb els problemes acertats i mostra el seu enunciat i el numero d'intents que ha necessitat l'usuari per acertar el problema.
    for (problema in llistaAcertats) {
        problema.mostraEnunciat()
        println("S'han necessitat ${problema.intents} per acertar el resultat d'aquest problema")
        println()
    }
}